## Chip

ESP32 Chip tools library for Arduino
===

Example of using
```

void setup() {
    Serial.printf("%s\r\n", Chip::getUniqueID().c_str());
}

void loop() {
}
```

----
This software is written by Tomas Hujer for various projects and is licensed under The MIT License.<br>
