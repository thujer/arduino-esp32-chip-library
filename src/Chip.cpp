/**
    Chip
    Chip.cpp
    Purpose: Chip API - Get unique ID

    @author Tomas Hujer
    @version 0.1 10/04/2019 
*/

#include "Chip.h"

char Chip::chipID[20];

Chip::Chip() {
}


uint64_t Chip::getDeviceId() {
    return ESP.getEfuseMac();
}


/**
 * Reset CPU
 */
void Chip::softwareRestart() {
	ESP.restart();
}


/**
 * Get chip serial number
 * In hexadecimal number ex.: 50954FA4AE30
 */
char* Chip::getDeviceIdHex() {

    uint64_t chipid;
            
    //The chip ID is essentially its MAC address(length: 6 bytes).
    chipid = ESP.getEfuseMac(); 
    
    sprintf(Chip::chipID, "%04X%08X", (uint16_t) (chipid >> 32), (uint32_t) chipid); //print High 2 bytes, then print Low 4bytes

    return Chip::chipID;
}


/**
 * Get chip serial number
 * In string format ex.: "50:95:4F:A4:AE:30"
 */
char* Chip::getDeviceIdString() {

    uint64_t chipid;
            
    //The chip ID is essentially its MAC address(length: 6 bytes).
    chipid = ESP.getEfuseMac(); 
    
    sprintf(Chip::chipID, "%01X:%01X:%01X:%01X:%01X:%01X", (uint8_t) (chipid >> 40), (uint8_t) (chipid >> 32), (uint8_t) (chipid >> 24), (uint8_t) (chipid >> 16), (uint8_t) (chipid >> 8), (uint8_t) chipid); //print High 2 bytes, then print Low 4bytes

    return Chip::chipID;
}


/**
 * Convert MAC to chip unique ID
 */
char* Chip::getUniqueID() {

	// TODO: use SHA1 to create unique hash
	//esp_sha(SHA1, &mac, sizeof(mac), &sha1HashBin[0]);

    //The chip ID is essentially its MAC address(length: 6 bytes).
    uint64_t chipid = ESP.getEfuseMac();
    char digit[3];
    byte size = 6;
            
    for(char ix=0; ix<size; ix++) {
        sprintf(digit, "%.2X", 0xFF - (unsigned char) (chipid >> ((size - ix - 1) * 8))); 
        sprintf(Chip::chipID + ix*2, "%c%c", digit[1], digit[0]); 
    }
    
    return Chip::chipID;
}


void Chip::printWakeUpReason() {
    
    esp_sleep_wakeup_cause_t wakeup_reason;

    wakeup_reason = esp_sleep_get_wakeup_cause();

    switch (wakeup_reason) {

        case ESP_SLEEP_WAKEUP_EXT0:
            Serial.println("Wakeup caused by external signal using RTC_IO");
            break;
        case ESP_SLEEP_WAKEUP_EXT1:
            Serial.println("Wakeup caused by external signal using RTC_CNTL");
            break;
        case ESP_SLEEP_WAKEUP_TIMER:
            Serial.println("Wakeup caused by timer");
            break;
        case ESP_SLEEP_WAKEUP_TOUCHPAD:
            Serial.println("Wakeup caused by touchpad");
            break;
        case ESP_SLEEP_WAKEUP_ULP:
            Serial.println("Wakeup caused by ULP program");
            break;
        default:
            Serial.printf("Wakeup was not caused by deep sleep: %d\n", wakeup_reason);
            break;
    }
}

