
#ifndef chip_h 
#define chip_h

#include <Arduino.h>

class Chip {
    private:
        
    public:
        Chip();
        static uint64_t getDeviceId();
        static char* getDeviceIdHex();
        static char* getDeviceIdString();
        char* getUniqueID();
        static char chipID[20];
        static void softwareRestart();
        static void printWakeUpReason();
};

#endif
